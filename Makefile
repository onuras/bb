
PROJECT = bb
VERSION = 0.1
RELEASE_DIR = ../bb_release

export: clean linux windows osx
	@echo BB ${VERSION} is ready!


clean:
	rm -rfv ../bb_release

linux:
	mkdir -pv ${RELEASE_DIR}/${PROJECT}-${VERSION}_linux
	godot --export "linux" ${RELEASE_DIR}/${PROJECT}-${VERSION}_linux/${PROJECT}-${VERSION}.x86_64
	cd ${RELEASE_DIR} && tar cjfv ${PROJECT}-${VERSION}_linux.tbz ${PROJECT}-${VERSION}_linux

windows:
	mkdir -pv ${RELEASE_DIR}/${PROJECT}-${VERSION}_windows
	godot --export "windows" ${RELEASE_DIR}/${PROJECT}-${VERSION}_windows/${PROJECT}-${VERSION}.exe
	cd ${RELEASE_DIR} && zip -r ${PROJECT}-${VERSION}_windows.zip ${PROJECT}-${VERSION}_windows

osx:
	mkdir -pv ${RELEASE_DIR}/${PROJECT}-${VERSION}_windows
	godot --export "osx" ${RELEASE_DIR}/${PROJECT}-${VERSION}_osx.zip
