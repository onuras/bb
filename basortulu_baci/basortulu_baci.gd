extends Area

onready var character = get_node("/root/Game/Character")
var speed = 1
var max_speed = 2.5

func _ready():
	speed = rand_range(1, max_speed)

func _physics_process(delta):
	if not character or character.game_over:
		return
	
	var target = character.get_translation() - get_translation()
	global_translate(target.normalized() * speed * delta)
	look_at(character.get_translation() , Vector3(0, 1, 0))

func _on_BasortuluBaci_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_pressed("shoot") and not character.game_over:
		# Shoot enemy here
		var character = get_node("/root/Game/Character")
		if character:
			character.target = get_translation()
			character.set_rotation = false

func _on_BasortuluBaci_area_entered(area):
	var name = area.get_name()
	if name == "Bullet" or name.find("Bullet") > 0:
		get_node("/root/Game/Character").add_score(floor(speed * 20))
		# Die or take damage here
		queue_free()
