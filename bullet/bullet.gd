extends Area

const MOTION_SPEED = 10
var target

func _physics_process(delta):
	if not target:
		return
	
	# Remove bullet if game is over
	if get_node("/root/Game/Character").game_over:
		queue_free()
	
	# This is for rotating bullet but
	# since our bullet is only a cube now it is not required
	#look_at(target.normalized(), Vector3(0, 1, 0))
	global_translate(target.normalized() * MOTION_SPEED * delta)


# if bullet gets out of screen remove it
func _on_VisibilityNotifier_screen_exited():
	queue_free()

# if bullet hits something remove it
func _on_Bullet_area_entered( area ):
	queue_free()
