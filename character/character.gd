extends Area


const MOTION_SPEED = 3.5
onready var anim_player = get_node("Meshes/AnimationPlayer")
onready var anim_player_sound = get_node("SoundAnimationPlayer")
onready var camera = get_node("Camera")
var target
var set_rotation = false
var shoot_modifier = false
var last_shoot_frame = 0
const SHOOT_FREQUENCY = 10   # Per frame
var score = 0
var game_over = false
var move = false


func _ready():
	anim_player.get_animation("Run").set_loop(true)
	_reset()

func _reset():
	score = 0
	game_over = false
	target = null
	set_translation(Vector3(0, 0, 0))
	update_score()
	for enemy in get_node("/root/Game/Enemies").get_children():
		enemy.queue_free()
	for i in range(9):
		spawn_enemy()

func _physics_process(delta):
	last_shoot_frame += 1
	
	if not target or game_over:
		return
	
	if Input.is_action_pressed("shoot") or (Input.is_action_pressed("move") and shoot_modifier):
		shoot(target)
		return
	
	if Input.is_action_pressed("move"):
		_move_with_click(delta)
	# TODO: This is for WASD movement but its not ready yet
	#elif (Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right") or
	#		Input.is_action_pressed("move_top") or Input.is_action_pressed("move_bottom")):
	#	_move_with_movement_keys(delta)
	else:
		anim_player.play("Idle")
		

func _move_with_click(delta):
	var character_translation = get_translation()
	var direction = target - character_translation
	global_translate(direction.normalized() * MOTION_SPEED * delta)
	if not anim_player.is_playing():
		anim_player.play("Run")
	
	if not anim_player_sound.is_playing():
		anim_player_sound.play("WalkingSound")
	
	if not set_rotation:
		get_node("Meshes").look_at(target, Vector3(0, 1, 0))
		set_rotation = true
	
	# FIXME: Need to extend vector here
	# Character is stopping when it reaches target
	#if character_translation.distance_to(target) < 0.5:
	#	pass

# TODO: This function is not ready yet, need to implement looking to direction
func _move_with_movement_keys(delta):
	var dir = Vector3()
	var cam_xform = get_node("Camera").get_global_transform()
	
	if (Input.is_action_pressed("move_top")):
		dir += -cam_xform.basis[2]
	if (Input.is_action_pressed("move_bottom")):
		dir += cam_xform.basis[2]
	if (Input.is_action_pressed("move_left")):
		dir += -cam_xform.basis[0]
	if (Input.is_action_pressed("move_right")):
		dir += cam_xform.basis[0]
	
	if dir:
		dir.y = 0
		global_translate(dir.normalized() * MOTION_SPEED * delta)
		if anim_player.get_current_animation() != "Run":
			anim_player.play("Run")
	else:
		anim_player.play("Idle")


func _input(event):
	if event.is_action_pressed("zoom_in"):
		var size = camera.get_fov() - 0.5 if camera.get_fov() > 20 else 20
		get_node("Camera").set_fov(size)
	if event.is_action_pressed("zoom_out"):
		var size = camera.get_fov() + 0.5 if camera.get_fov() < 40 else 40
		get_node("Camera").set_fov(size)
	if event.is_action_pressed("shoot_modifier"):
		shoot_modifier = true
	if event.is_action_released("shoot_modifier"):
		shoot_modifier = false


func _on_GroundArea_input_event(camera, event, click_position, click_normal, shape_idx):
	#if event is InputEventMouseMotion:
	#	get_node("Meshes").look_at(click_position, Vector3(0, 1, 0))
	
	if (Input.is_action_pressed("shoot") or Input.is_action_pressed("move")) and not game_over:
		target = click_position
		set_rotation = false


func shoot(target):
	target.y = 0
	get_node("Meshes").look_at(target, Vector3(0, 1, 0))
	
	# Don't shoot every frame
	if last_shoot_frame < SHOOT_FREQUENCY:
		return
	
	var character_translation = get_translation()
	var bullet = preload("res://bullet/bullet.tscn").instance()
	
	# Set bullet position to guns point
	bullet.target = target - character_translation
	var bullet_spawn_point = get_node("Meshes/P90/BulletSpawnPoint")
	bullet.translate(bullet_spawn_point.to_global(bullet_spawn_point.get_translation()))
	get_node("/root/Game").add_child(bullet)
	anim_player.play("Idle")
	anim_player_sound.play("ShootSound")
	last_shoot_frame = 0


func update_score():
	get_node("UI/Score").set_text(String(score))
	spawn_enemy()

func add_score(n):
	score += n
	update_score()

func spawn_enemy():
	var enemy = preload("res://basortulu_baci/basortulu_baci.tscn").instance()
	get_node("/root/Game/Enemies").call_deferred("add_child", enemy)
	
	var pos = get_translation()
	var pos_location = randi() % 4
	var x = rand_range(5, 10)
	var y = rand_range(5, 10)
	if pos_location == 0:
		pos += Vector3(x, 0, y)
	elif pos_location == 1:
		pos += Vector3(-x, 0, -y)
	elif pos_location == 2:
		pos += Vector3(x, 0, -y)
	elif pos_location == 3:
		pos += Vector3(-x, 0, y)
	
	enemy.global_translate(pos)
	# increase difficulty based on score
	enemy.max_speed += (score + 1) / 10000

func _on_Character_area_entered(area):
	game_over = true
	anim_player.play("Idle")
	get_node("UI/GameOverScreen").set_visible(true)


func _on_Button_pressed():
	get_node("UI/GameOverScreen").set_visible(false)
	_reset()
