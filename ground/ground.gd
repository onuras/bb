extends GridMap

const INIT_GRID_MAX = 30


func _ready():
	for i in range(-INIT_GRID_MAX, INIT_GRID_MAX):
		for j in range(-INIT_GRID_MAX, INIT_GRID_MAX):
			set_cell_item(i, 0, j, 0)