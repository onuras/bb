extends Spatial

var loader

func _process(delta):
	if not loader:
		return
	
	var err = loader.poll()
	
	if err == ERR_FILE_EOF:
		var resource = loader.get_resource().instance()
		loader = null
		get_node("/root").add_child(resource)
		queue_free()

func _on_Button_pressed():
	var button = get_node("MarginContainer/VBoxContainer/Button")
	button.set_disabled(true)
	button.set_text("Loading")
	
	loader = ResourceLoader.load_interactive("res://game/game.tscn")